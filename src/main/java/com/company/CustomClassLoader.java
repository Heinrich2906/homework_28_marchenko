package com.company;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by heinr on 22.01.2017.
 */
public class CustomClassLoader extends ClassLoader {

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        Class<?> result = findLoadedClass(name);
        if (result == null) {
            try {
                result = getParent().loadClass(name);
                return result;
            } catch (Exception e) {
                return loadClassFromResources(name);
            }
        } else {
            return result;
        }
    }

    private Class<?> loadClassFromResources(String name) throws ClassNotFoundException {
        String classPath = name.replaceAll("\\.", "/") + ".class";
        byte[] bytecode = null;
        Path fullClassPath = Paths.get("resources/" + classPath);
        if (Files.exists(fullClassPath)) {
            try {
                bytecode = Files.readAllBytes(fullClassPath);
            } catch (IOException e) {
                throw new RuntimeException("Cannot read file " + fullClassPath);
            }
        } else {
            String javaPath = name.replaceAll("\\.", "/") + ".java";
            Path fullJavaPath = Paths.get("resources/" + javaPath);
            if (Files.exists(fullJavaPath)) {
                JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
                if (compiler.run(null, null, null, fullJavaPath.toFile().getAbsolutePath()) == 0) {
                    try {
                        bytecode = Files.readAllBytes(fullClassPath);
                    } catch (IOException e) {
                        throw new RuntimeException("Cannot read file " + fullClassPath);
                    }
                } else {
                    throw new RuntimeException("Cannot compile class: " + name);
                }
            } else {
                throw new ClassNotFoundException("File " + fullJavaPath + " not found");
            }
        }
        if (bytecode != null) {
            return defineClass(name, bytecode, 0, bytecode.length);
        } else {
            throw new ClassNotFoundException("Class " + name + " not found");
        }
    }

    public static void main(String[] args) {
        CustomClassLoader classLoader = new CustomClassLoader();
        try {
            Class<?> hw_28_Class = classLoader.loadClass("com.myclasses.Exercise");

            //Aufgabe 1.

            //Получение публичных полей
            System.out.println("Получение публичных полей");
            Field[] fields = hw_28_Class.getFields();

            for (Field s : fields) {
                System.out.println(s.toString());
            }

            separator();

            //Получение всех полей, кроме унаследованных
            System.out.println("Получение всех полей, кроме унаследованных");
            Field[] declaredFields = hw_28_Class.getDeclaredFields();

            for (Field s : declaredFields) {
                System.out.println(s.toString());
            }

            separator();

            //Получение всех публичных методов
            System.out.println("Получение всех публичных методов");
            Method[] methods = hw_28_Class.getMethods();

            for (Method s : methods) {
                System.out.println(s.toString());
            }

            separator();

            //Получение всех методов, кроме унаследованных
            System.out.println("Получение всех методов, кроме унаследованных");
            Method[] declaredMethods = hw_28_Class.getDeclaredMethods();

            for (Method s : declaredMethods) {
                System.out.println(s.toString());
            }

            separator();

            //Aufgabe 2.
            Constructor constructor = hw_28_Class.getConstructor();

            Object object = constructor.newInstance();
            fields = object.getClass().getDeclaredFields();
            for (Field s : fields) {
                String fieldName = s.getName();
                Type fieldType = s.getType();
                if (fieldType.equals(String.class)) {
                    boolean oldAccessible = s.isAccessible();
                    s.setAccessible(true);
                    String fieldValue = (String) s.get(object);
                    s.setAccessible(oldAccessible);

                }
            }
            separator();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void separator() {
        System.out.println("------------------------\n");
    }

    private static void printParameterTypes(Class[] paramTypes) {
        if (paramTypes.length == 0) {
            System.out.println("<Empty params>");
        } else {
            for (Class paramType : paramTypes) {
                System.out.print(paramType.getName());
            }
            System.out.println();
        }
    }
}


